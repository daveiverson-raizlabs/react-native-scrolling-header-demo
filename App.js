import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import RootNavigator from './RootNavigator';
import SideMenu from 'react-native-side-menu';

const Menu = () => (
  <View><Text>Sidebar</Text></View>
)

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { menuIsOpen: false }
  }

  render() {
    const screenProps = {
      toggleMenu: () => {
        console.log('toggling')
        this.setState({ menuIsOpen: !this.state.menuIsOpen }) 
      },
      menuIsOpen: this.state.menuIsOpen,
    };
    const menu = <Menu navigator={navigator}/>;
    const isOpen = true;

    return (
      <SideMenu autoClosing disableGestures isOpen={this.state.menuIsOpen} menu={menu} onChange={(isOpen) => { this.setState({ ...this.state, menuIsOpen: isOpen }) }}>
        <RootNavigator screenProps={screenProps} z={screenProps} />
      </SideMenu>
    );
  }
}

