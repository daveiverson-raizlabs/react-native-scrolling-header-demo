import React from 'react';
import { StyleSheet, Text, View, Button, ScrollView, Animated, Image } from 'react-native';
import ScreenWithHeader from '../components/ScreenWithHeader';
import { lotsOfContent } from '../utils';

class HomeScreen extends React.Component {

  componentWillMount(){
    console.log('mounting home screen')
  }
  componentWillUnmount(){
    console.log('unmounting home screen')
  }
  componentWillUpdate(){
    console.log('updating home screen');
  }

  shouldComponentUpdate(nextProps, nextState){
    console.log('checking if home screen should update')
    return true;
  }

  render() {
    console.log('home screen rendering');
    return (
      <ScreenWithHeader
        headerType="" 
        shouldScroll 
        viewProps={{
          style: { backgroundColor: "#bbbbbb"}
        }}
        headerProps={{
          transparent: true,
          title: "Home",
          idCardButton: true,
          searchButton: true,
          navigation: this.props.navigation,
          toggleMenu: this.props.screenProps.toggleMenu,
        }}
      >
        <Image
          style={{position: 'absolute', top: 0, right: 0}}
                  resizeMode="cover"
                  source={require('../images/img-dashboard-logo-watermark.png')}
                />
        <View style={{backgroundColor: 'transparent',}}>
          <Text>Home Screen</Text>
          <Text>The menu is: {this.props.screenProps.menuIsOpen ? 'open' : 'closed'}</Text>
          <Button
            onPress={() => this.props.navigation.navigate('Details')}
            title="Go to Details screen"
          />
          <Button
            onPress={() => this.props.navigation.navigate('About')}
            title="Go to About screen"
          />

        <Button
            onPress={() => this.props.navigation.navigate('Contact')}
            title="Go to Contact screen"
          />

        <Button
            onPress={() => this.props.navigation.navigate('FAQ')}
            title="Go to FAQ screen"
          />

          {lotsOfContent()}
        </View>
      </ScreenWithHeader>
    );
  }
}

export default HomeScreen;
