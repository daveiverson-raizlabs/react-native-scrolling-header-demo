import React from 'react';
import { NavigationActions } from 'react-navigation';
import { StyleSheet, Text, View, Button, ScrollView, Animated } from 'react-native';
import ScreenWithHeader from '../components/ScreenWithHeader';
import { lotsOfContent } from '../utils';

class DetailsScreen extends React.Component {
  componentWillMount(){
    console.log('mounting details screen');
  }
  componentWillUnmount(){
    console.log('unmounting details screen');
  }
  componentWillUpdate(){
    console.log('updating details screen');
  }

  render(){
    return (<ScreenWithHeader 
    headerType="DETAIL" 
    shouldScroll 
    headerProps={{
      title: "Details",
      leftButton: 'BACK',
      ctaButton: {
        action: () => {console.log('cta button clicked')},
        title: 'DONE',
      },
      graphic: require('../images/img-roadside-assistance-header-illustration.png'),
      navigation: this.props.navigation,
      toggleMenu: this.props.screenProps.toggleMenu,
    }}
    >
      <Text>Details Screen</Text>
      <Button
        onPress={() => { this.props.navigation.goBack() } }
        title="Go back"
      />

      <Button
        onPress={() => { this.props.navigation.navigate('Home') } }
        title="Go home"
      />

      <Button
        onPress={() => { this.props.navigation.dispatch(NavigationActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({ routeName: 'Details'})
          ]
          }))}}
        title="Reset stack"
      />

      <Button
        onPress={() => { this.props.navigation.dispatch(NavigationActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({ routeName: 'Home'})
        ]
        }))}}
        title="Go home with a stack reset"
      />
      {lotsOfContent()}
    </ScreenWithHeader>
  )}
}

export default DetailsScreen;
