import React from 'react';
import { NavigationActions } from 'react-navigation';
import { StyleSheet, Text, View, Button, ScrollView, Animated } from 'react-native';
import ScreenWithHeader_RenderProps from '../components/ScreenWithHeader_RenderProps';
import { lotsOfContent } from '../utils';

class AboutScreen extends React.Component {
  renderScreen = (paddingComponent, scrollProps) => {
    return (
    <Animated.ScrollView {...scrollProps}>
      {paddingComponent}
      <Text>About Screen</Text>
      <Button
        onPress={() => { this.props.navigation.goBack() } }
        title="Go back"
      />
      {lotsOfContent()}
    </Animated.ScrollView>
    )
  }
  render() {
    return (<ScreenWithHeader_RenderProps
      headerType="STANDARD"
      headerProps={{
        title: "About",
        leftButton: 'BACK',
        ctaButton: {
          action: () => {console.log('cta button clicked')},
          title: 'DONE',
        },
        navigation: this.props.navigation,
        toggleMenu: this.props.screenProps.toggleMenu,
      }}
      renderScreen={this.renderScreen}
    />);
  }
}

export default AboutScreen;
