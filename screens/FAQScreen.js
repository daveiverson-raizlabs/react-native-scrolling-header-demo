import React from 'react';
import { NavigationActions } from 'react-navigation';
import { StyleSheet, Text, View, Button, ScrollView, Animated } from 'react-native';
import ScreenWithHeader_RenderProps_Alternate from '../components/ScreenWithHeader_RenderProps_Alternate';
import { lotsOfContent } from '../utils';
import HeaderShort from '../components/HeaderShort';
import HeaderTall from '../components/HeaderTall';

class FAQScreen extends React.Component {

  renderHeader = (scrollProps) => <HeaderTall
    title="FAQs"
    leftButton="BACK"
    ctaButton={{
      action: () => {console.log('cta button clicked')},
      title: 'DONE',
    }}
    navigation={this.props.navigation}
    toggleMenu={this.props.screenProps.toggleMenu}
    {...scrollProps}
  />
  
  render() {
    return (<ScreenWithHeader_RenderProps_Alternate renderHeader={this.renderHeader} headerHeight={200} >
      {(paddingComponent, scrollProps) => (
          <Animated.ScrollView {...scrollProps}>
            {paddingComponent}
            <Text>FAQs Screen</Text>
            <Button
              onPress={() => { this.props.navigation.goBack() } }
              title="Go back"
            />
            {lotsOfContent()}
          </Animated.ScrollView>
        )}
    </ScreenWithHeader_RenderProps_Alternate>);
  }
}

export default FAQScreen;
