import React from 'react';
import { NavigationActions } from 'react-navigation';
import { StyleSheet, Text, View, Button, ScrollView, Animated } from 'react-native';
import ScreenWithHeader_HOC from '../components/ScreenWithHeader_HOC';
import { lotsOfContent } from '../utils';

class ContactScreenContentsRaw extends React.Component {
  render() {
    return (
      <Animated.ScrollView {...this.props.scrollProps}>
        {this.props.paddingComponent}
        <Text>About Screen</Text>
        <Button
          onPress={() => { this.props.navigation.goBack() } }
          title="Go back"
        />
        {lotsOfContent()}
      </Animated.ScrollView>
      )
  }
}

const ContactScreenContents = ScreenWithHeader_HOC(ContactScreenContentsRaw);

class ContactScreen extends React.Component {
  render() {
    return (
      <ContactScreenContents
        headerType="STANDARD"
        headerProps={{
          title: "Contact",
          leftButton: 'BACK',
          ctaButton: {
            action: () => {console.log('cta button clicked')},
            title: 'DONE',
          },
          navigation: this.props.navigation,
          toggleMenu: this.props.screenProps.toggleMenu,
      }}
      navigation={this.props.navigation}
      />
    )
  }
}

export default ContactScreen;