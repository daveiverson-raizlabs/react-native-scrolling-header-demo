import React from 'react';
import { StyleSheet, Text, View, Button, ScrollView, Animated, Image } from 'react-native';
import HeaderShort from './HeaderShort';
import HeaderTall from './HeaderTall';
import HeaderPadding from './HeaderPadding';

/**
 * A container component that renders the header and screen View/ScrollView
 * The ScrollView requires a padding component and onScroll property: we don't want to manually set these up on every screen
 * 
 * 
 * PROPS
 * headerType: type of header to use (DETAIL or STANDARD)
 * headerProps: props to be passed to the header
 */

let ScreenWithHeader_HOC = (ScreenComponent) => class ScreenWithHeader2 extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      scrollYPosition: new Animated.Value(0),
      scrollPositionValue: 0,
    }
  }

  onScrollHandler = (event) => {
    const scrollPos = event.nativeEvent.contentOffset.y;
    this.setState({
        ...this.state,
        scrollPositionValue: scrollPos,
      });
  } 

  componentWillUnmount() {
    this.state.scrollYPosition.removeAllListeners()
  }

  scrollProps = () => {
    let scrollThrottle = 1;
    let onScrollOptions = {
      useNativeDriver: true,
    };
    if(this.headerIsTall(this.props.headerType)){
      let scrollThrottle = 60;
      // listen to the scroll position so we can pass it to the header (along with the animated version)
      // can't use native driver because the height changes
      onScrollOptions = { useNativeDriver: false, listener: this.onScrollHandler };
    }

    let event = Animated.event
      (
        [{ nativeEvent: { contentOffset: { y: this.state.scrollYPosition } } }],
        onScrollOptions,
    )
    return {
      onScroll: event,
      scrollEventThrottle: scrollThrottle
    }
  }

  renderHeader = () => {
    let height = this.headerHeight();
    let opacity = this.headerOpacity();
    let hasShadow = false;
    let { headerType } = this.props;
    switch(headerType) {
      case "DETAIL":
        let isShort = this.state.scrollPositionValue > 20 ? true : false;
        return <HeaderTall height={height} opacity={opacity} isShort={isShort}  {...this.props.headerProps} />
      default:
        return <HeaderShort opacity={opacity} {...this.props.headerProps} />
    }
  }

  headerOpacity = () => {
    return this.state.scrollYPosition.interpolate({
      inputRange: [0, 20],
      outputRange: [0.0, 1.0],
      extrapolate: 'clamp',
    });
  }

  headerHeight = () => {
    if(this.headerIsTall(this.props.headerType)){
      return this.state.scrollYPosition.interpolate({
        inputRange: [0, 50],
        outputRange: [200, 100],
        extrapolate: 'clamp',
      });
    }
    return 100;
  }

  headerIsTall = (headerType) => {
    switch(headerType){
      case 'SPLASH':
      case 'DETAIL':
        return true;
      default:
        return false;
    }
  }

  render() {
    let screenProps = {
      paddingComponent: <HeaderPadding height={this.headerHeight()} />,
      scrollProps: this.scrollProps(),
    }
    return (
      <View>
        <ScreenComponent {...screenProps} {...this.props} />
        {this.renderHeader()}
      </View>
    );
  };
};

export default ScreenWithHeader_HOC;
