import React from 'react';
import { Animated } from 'react-native';

// padding component that keeps the screen content from going underneath the header

const HeaderPadding = ({ height }) => {
  return (<Animated.View style={{ height: height, width: '100%' }} />);
}

export default HeaderPadding;
