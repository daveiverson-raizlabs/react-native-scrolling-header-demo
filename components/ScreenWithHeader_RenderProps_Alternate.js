import React from 'react';
import { StyleSheet, Text, View, Button, ScrollView, Animated, Image } from 'react-native';
import HeaderShort from './HeaderShort';
import HeaderTall from './HeaderTall';
import HeaderPadding from './HeaderPadding';

/**
 * A container component that renders the header and screen View/ScrollView
 * The ScrollView requires a padding component and onScroll property: we don't want to manually set these up on every screen
 * 
 * 
 * PROPS
 * renderHeader: function(scrollProps) that renders the header
 * children: function(paddingComponent, scrollProps) that renders the screen
 * headerHeight: number: the height of the header (if not using the standard height)
 */

 const standardHeaderHeight = 100;

class ScreenWithHeader_RenderProps_Alternate extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      scrollYPosition: new Animated.Value(0),
      scrollPositionValue: 0,
    }
  }

  onScrollHandler = (event) => {
    const scrollPos = event.nativeEvent.contentOffset.y;
    this.setState({
        ...this.state,
        scrollPositionValue: scrollPos,
      });
  }

  componentWillUnmount() {
    this.state.scrollYPosition.removeAllListeners()
  }

  scrollProps = () => {
    let scrollThrottle = 1;
    let onScrollOptions = {
      useNativeDriver: true,
    };
    if(this.props.headerHeight !== standardHeaderHeight){
      let scrollThrottle = 60;
      // listen to the scroll position so we can pass it to the header (along with the animated version)
      // can't use native driver because the height changes
      onScrollOptions = { useNativeDriver: false, listener: this.onScrollHandler };
    }

    let event = Animated.event
      (
        [{ nativeEvent: { contentOffset: { y: this.state.scrollYPosition } } }],
        onScrollOptions,
    )
    return {
      onScroll: event,
      scrollEventThrottle: scrollThrottle
    }
  }

  headerOpacity = () => {
    return this.state.scrollYPosition.interpolate({
      inputRange: [0, 20],
      outputRange: [0.0, 1.0],
      extrapolate: 'clamp',
    });
  }

  headerHeight = () => {
    if(this.props.headerHeight !== standardHeaderHeight){
      return this.state.scrollYPosition.interpolate({
        inputRange: [0, 50],
        outputRange: [this.props.headerHeight, standardHeaderHeight],
        extrapolate: 'clamp',
      });
    }
    return standardHeaderHeight;
  }

  render() {
    let paddingComponent = <HeaderPadding height={this.headerHeight()} />;
    let scrollProps = this.scrollProps()
    let headerProps = {
      height: this.headerHeight(),
      opacity: this.headerOpacity(),
      hasShadow: false,
    }
    if(this.props.headerHeight !== standardHeaderHeight){
      // if the height is going to change, figure out whether to show the tall or short version of the header
      headerProps.isShort = this.state.scrollPositionValue > 20 ? true : false;
    }
    return (
      <View>
        {this.props.children(paddingComponent, scrollProps)}
        {this.props.renderHeader(headerProps)}
      </View>
    );
  };
};

ScreenWithHeader_RenderProps_Alternate.defaultProps = {
  renderHeader: () => {},
  headerHeight: standardHeaderHeight,
}

export default ScreenWithHeader_RenderProps_Alternate;
