import React from 'react';
import { StyleSheet, Text, View, Button, ScrollView, Animated } from 'react-native';
import { NavigationActions } from 'react-navigation'
import styles from '../styles';

/**
 * Short header component: while scrolling it changes opacity, but not height
 * 
 * 
 * PROPS:
 * toggleMenu: function to be called when the menu button is pressed
 * navigation: the React Navigation navigation state
 * leftButton: the type of left action button to use (BACK, MENU, CLOSE)
 * ctaButton: the right action button. should have a 'title' property and 'action' property
 * searchButton: boolean: whether to show a right search button
 * idCardButton: boolean: wheather to show a right id card button
 * opacity: the opacity of the menu background and shadow. Usually will be an animated value
 * transparent: boolean: should the background of the header transparent
 * title: the title to be shown in the header
 */

class HeaderShort extends React.Component {
  leftButton = () => {
    switch(this.props.leftButton){
      case 'BACK':
        return <Button onPress={() => { this.props.navigation.dispatch(NavigationActions.back())  }} title="Back" />;
      case 'MENU':
      default:
        return <Button onPress={this.props.toggleMenu} title="Menu" />
    }
  }
  rightButtons = () => {
    const { ctaButton, searchButton, idCardButton } = this.props;
    let buttons = [];
    if(searchButton){
      buttons.push(<Button onPress={() => {}} title="ID Card" key="searchButton" />);
    }
    if(idCardButton){
      buttons.push(<Button onPress={() => {}} title="Search" key="idCardButton" />);
    }
    if(ctaButton){
      buttons.push(<Button onPress={ctaButton.action} title={ctaButton.title} key="ctaButton" />);
    }
    return (
      <View style={styles.rightButtonContainerStyles}>{buttons}</View>
    )
  }
  render() {
    let { opacity } = this.props;
    const height = 100;
    return (
      <View style={[styles.containerStyles, { height }]}>
        {/* The white background. Nested component has the shadow (shadows fail on Animated.View) */}
        <Animated.View style={[styles.headerBGStyles, { opacity }]} >
          <View style={styles.intBGStyles} />
        </Animated.View>
        {/* Content */}
        <View style={[styles.headerStyles, this.props.transparent ? {} : styles.opaqueHeaderStyles ]}>
          {this.leftButton()}
          <Text>{this.props.title}</Text>
          <View>
            {this.rightButtons()}
          </View>
        </View>
      </View>
    )
  }
}

export default HeaderShort;
