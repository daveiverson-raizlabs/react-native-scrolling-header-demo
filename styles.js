import {StyleSheet, Platform} from 'react-native';

const styles = StyleSheet.create({
  containerStyles: {
    position: 'absolute', 
    left: 0, 
    right: 0, 
    top: 0,
  },
  
  headerStyles: {
    justifyContent: 'space-between', 
    alignContent: 'flex-end',
    alignItems: 'flex-end',
    position: 'absolute', 
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    backgroundColor: 'transparent',
    height: '100%',
    // elevation needs to be higher than headerBGStyles so it says on top
    elevation: 6,

    flexDirection: 'row',
  },
  opaqueHeaderStyles: {
    // on Android, solid colored backgrounds with elevation will always show a shadow
    // to avoid this extra shadow, only allow opaque headers on iOS
    backgroundColor: (Platform.OS === 'ios') ? 'white' : 'transparent',
  },
  headerBGStyles: {
    backgroundColor: 'white',
    position: 'absolute', 
    left: 0, 
    right: 0, 
    top: 0, 
    bottom: 0,
    width: '100%',
    height: '100%',
    // elevation adds shadow
    elevation: 5,
  },
  intBGStyles: {
    shadowColor: 'black',
    shadowOffset: { height: 5, width: 0 },
    shadowOpacity: 0.3,
    flex: 1,
    width: '100%',
    height: '100%',
  },
  rightButtonContainerStyles: {
    flexDirection: 'row'
  }
});

export default styles;
