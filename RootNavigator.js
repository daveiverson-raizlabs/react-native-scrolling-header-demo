import React from 'react';
import { StackNavigator } from 'react-navigation';
import { StyleSheet, Text, View, Button, ScrollView, Animated } from 'react-native';
import HomeScreen from './screens/HomeScreen';
import DetailsScreen from './screens/DetailsScreen';
import AboutScreen from './screens/AboutScreen';
import ContactScreen from './screens/ContactScreen';
import FAQScreen from './screens/FAQScreen';

const RootNavigator = StackNavigator({
  Home: {
    screen: HomeScreen,
    navigationOptions: {
      headerTitle: 'Home',
      header: null,
    },
  },
  Details: {
    screen: DetailsScreen,
    navigationOptions: {
      headerTitle: 'Details',
      header:  null,
    },
  },
  About: {
    screen: AboutScreen,
    navigationOptions: {
      headerTitle: 'About',
      header:  null,
    },
  },
  Contact: {
    screen: ContactScreen,
    navigationOptions: {
      headerTitle: 'Contact',
      header:  null,
    },
  },
  FAQ: {
    screen: FAQScreen,
    navigationOptions: {
      headerTitle: 'FAQ',
      header:  null,
    },
  },
},{
  cardStyle: {
    backgroundColor: '#fff'
  }
});

export default RootNavigator;